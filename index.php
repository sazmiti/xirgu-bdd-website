<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Xirgu's database</title>
    <?php
    session_start();
    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/css.php";
    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/en_bdd.php";
    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/esp_bdd.php";
    $query = $dbEN->prepare("SELECT relname,n_live_tup FROM pg_stat_user_tables ORDER BY n_live_tup DESC;");
    $query->execute();
    $statsTableEN = $query->fetchAll();
    $query = $dbEN->prepare("SELECT SUM(n_live_tup) as nbrows FROM pg_stat_user_tables;");
    $query->execute();
    $nbrowsEN = $query->fetch();
    $query = $dbEN->prepare("SELECT pg_size_pretty(pg_database_size('".$settingsEN["dbname"]."')) as size;");
    $query->execute();
    $sizeOfTableEN = $query->fetch();
    $query = $dbESP->prepare("SELECT relname,n_live_tup FROM pg_stat_user_tables ORDER BY n_live_tup DESC;");
    $query->execute();
    $statsTableESP = $query->fetchAll();
    $query = $dbESP->prepare("SELECT SUM(n_live_tup) as nbrows FROM pg_stat_user_tables;");
    $query->execute();
    $nbrowsESP = $query->fetch();
    $query = $dbEN->prepare("SELECT pg_size_pretty(pg_database_size('".$settingsESP["dbname"]."')) as size;");
    $query->execute();
    $sizeOfTableESP = $query->fetch();
    ?>

</head>

<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/home_nav.php"; ?>

<div class="container pt-3">
    <div class="card">
        <div class="card-body">
            <p class="card-text">Welcome ! This databases were made by <em>Samuel Vannier</em> and <em>Thomas Pélissié de Montémont</em></p>
        </div>
    </div>

    <div class="d-flex justify-content-between pt-3">
        <div class="card" style="width: 45%">
            <div class="card-body">
                <h5 class="card-title">Statistics English database</h5>
                <canvas id="en_stats_graph"></canvas>
                <div class="card-body">
                    <p>Number of rows in database : <b><?php echo $nbrowsEN["nbrows"]?></b></p>
                    <p>Size of the database : <b><?php echo $sizeOfTableEN["size"]?></b></p>
                </div>
            </div>
        </div>

        <div class="card" style="width: 45%">
            <div class="card-body">
                <h5 class="card-title">Statistics Spanish database</h5>
                <canvas id="esp_stats_graph"></canvas>
                <div class="card-body">
                    <p>Number of rows in database : <b><?php echo $nbrowsESP["nbrows"]?></b></p>
                    <p>Size of the database : <b><?php echo $sizeOfTableESP["size"]?></b></p>
                </div>
            </div>

        </div>
    </div>
</div>

</body>

<script>
    new Chart(document.getElementById("en_stats_graph"), {
        type: 'bar',
        data: {
            "labels": [
            <?php
                foreach ($statsTableEN as $row){
                    echo '"'. $row["relname"] . '",';
                }
            ?>],
            "datasets": [{
                "label": "Number of rows in tables",
                "data": [
                    <?php
                    foreach ($statsTableEN as $row){
                        echo '"'. $row["n_live_tup"] . '",';
                    }
                    ?>],
                "fill": false,
                "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)", "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)","rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)", "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)"],
                "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)", "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)","rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)", "rgb(75, 192, 192)", "rgb(54, 162, 235)"],
                "borderWidth": 1
            }]
        },
        "options": {"scales": {"yAxes": [{"ticks": {"beginAtZero": true}}]}}
    });

    new Chart(document.getElementById("esp_stats_graph"), {
        type: 'bar',
        data: {
            "labels": [
                <?php
                foreach ($statsTableESP as $row){
                    echo '"'. $row["relname"] . '",';
                }
                ?>],
            "datasets": [{
                "label": "Number of rows in tables",
                "data": [
                    <?php
                    foreach ($statsTableESP as $row){
                        echo '"'. $row["n_live_tup"] . '",';
                    }
                    ?>],
                "fill": false,
                "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)", "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)","rgba(255, 98, 132, 0.2)", "rgba(255, 158, 64, 0.2)", "rgba(255, 204, 86, 0.2)", "rgba(75, 191, 192, 0.2)", "rgba(54, 161, 235, 0.2)"],
                "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)", "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)","rgb(255, 98, 132)", "rgb(255, 158, 64)", "rgb(255, 204, 86)", "rgb(75, 191, 192)", "rgb(54, 161, 235)"],
                "borderWidth": 1
            }]
        },
        "options": {"scales": {"yAxes": [{"ticks": {"beginAtZero": true}}]}}
    });
</script>

</html>


