<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Xirgu's database</title>
    <?php
    session_start();
    if (isset($_SESSION['id'])){
        header("Location: /index.php");
    }
    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/css.php";
    ?>

</head>

<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/home_nav.php"; ?>

<div class="container pt-3 d-flex justify-content-center">
    <div class="d-flex w-50 justify-content-center flex-column card p-3">
        <h1 class="text-center">Sign In</h1>
        <form action="dependencies/UsersManagement/signinScript.php" method="post" class="d-flex justify-content-center flex-column">
            <div class="input-group mb-3 w-auto mx-3 ">
                <span class="input-group-text" id="basic-addon1" style="width: fit-content">Username</span>
                <input type="text" class="form-control w-auto" aria-describedby="basic-addon1" name="username">
            </div>
            <div class="input-group mb-3 w-auto mx-3">
                <span class="input-group-text" id="basic-addon1" style="width: fit-content">Password</span>
                <input type="password" class="form-control w-auto" aria-describedby="basic-addon1" name="pwd">
            </div>
            <button name="submit" type="submit" class="btn btn-success">Sign In</button>
        </form>
        <a href="signup.php" class="link-info text-center">Not already sign up ? Click here</a>
    </div>
</div>

</body>
</html>


