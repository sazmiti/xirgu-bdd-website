<?php
include_once "configDB.php";
try {
    $dbESP = new PDO(
        $settingsESP["driver"].":host=". $settingsESP["host"] .";port=". $settingsESP["port"] .";dbname=".$settingsESP["dbname"],
        $settingsESP["user"],
        $settingsESP["password"]
    );
}catch (PDOException $e){
    echo "Erreur : ". $e->getMessage();
    die();
}