<?php

session_start();

if (isset($_POST['submit'])) {

    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/account_bdd.php";

    $username = htmlspecialchars($_POST['username']);
    $pwd = htmlspecialchars($_POST['pwd']);

//ERROR CHECKER
    //CHECK FOR EMPTY

    if (empty($username) || empty($pwd)) {
        header("Location: ../../signin.php?login=empty");
        exit();
    } else {
        //CHECK IF THE EMAIL EXIST

        $sql = "SELECT * FROM users WHERE username = '$username';";
        $query = $dbAccounts->prepare($sql);
        $query->execute();
        $result = $query->fetch();
        $resultcheck = $query->rowCount();


        if ($resultcheck < 1) {
            header("Location: ../../signin.php?login=error");
            exit();
        } else {

            if ($result) {
                //DECODE THE PASSWORD
                $hashedpwdcheck = password_verify($pwd, $result['password']);

                if ($hashedpwdcheck == false) {
                    header("Location: ../../signin.php?login=error");
                    exit();
                } elseif ($hashedpwdcheck == true) {
                    //CONNECTION
                    $_SESSION['id'] = $result['id'];
                    $_SESSION['username'] = $result['username'];
                    $_SESSION['role'] = $result['role'];

                    header("Location: ../../index.php");
                    exit();
                }
            }
        }
    }
} else {
    header("Location: ../../signin.php?login=error");
    exit();
}
