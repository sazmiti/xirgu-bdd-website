<?php

if (isset($_POST['submit'])) {

    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/account_bdd.php";

    $username = htmlspecialchars($_POST['username']);
    $pwd = htmlspecialchars($_POST['pwd']);
    $role = htmlspecialchars($_POST['role']);

//ERROR CHECKER
    //CHECK FOR EMPTY
    if (empty($username) || empty($pwd) || empty($role)) {
        header("Location: ../../signup.php?signup=empty");
        exit();
    } else {
        //CHECK FOR INVALID FORMAT
        if (!preg_match("/^[a-zA-Z0-9_\-]*$/", $username)) {
            header("Location: ../../signup.php?signup=invalid");
            exit();
        } else {
            //CHECK IF THE USERNAME IS TAKEN
            $sql = "SELECT * FROM users WHERE username = '$username'";
            $query = $dbAccounts->prepare($sql);
            $query->execute();
            $resultcheck = $query->rowCount();

            if ($resultcheck > 0) {
                header("Location: ../../signup.php?signup=usernameTaken");
                exit();
            } else {
                //ALL THE FIELD ARE RIGHT AND VERIFIED SO CAN ENTER TO THE DATABASE
                //ENCODE PASSWORD
                $hashedpwd = password_hash($pwd, PASSWORD_DEFAULT);

                //GO TO THE DATABASE
                $sql = "INSERT INTO users (username, password, role) VALUES ('$username', '$hashedpwd', '$role');";
                $query = $dbAccounts->prepare($sql);
                $query->execute();

                //EXIT THE FILE
                header("Location: ../../signup.php?signup=success");
                exit();
            }
        }
    }
} else {
    header("Location: ../../signup.php");
    exit();
}