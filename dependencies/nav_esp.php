<nav class="navbar navbar-expand-lg navbar-light bg-light bg-white border-bottom shadow-sm">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Xirgu's Database</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/esp/dashboard.php">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/esp/display_resource.php">Display</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/esp/statistics.php">Statistics</a>
                </li>
                <?php if ($_SESSION["role"] == "admin") :?>
                    <li class="nav-item">
                        <a class="nav-link" href="/esp/edit_resource.php">Edit</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/esp/insert_resource.php">Insert</a>
                    </li>
                <?php endif; ?>
            </ul>

            <ul class="navbar-nav ms-auto">
                <a href="<?= str_replace("/esp/","/en/", $_SERVER["REQUEST_URI"]) ?>" class="m-auto"><i class="flag flag-es"></i></a>
                <?php if(isset($_SESSION['id'])): ?>
                    <li class="nav-item">
                        <form action="../dependencies/UsersManagement/logout.php" method="post" class="nav-link m-0">
                            <button class="btn btn-outline-danger" name="submit" type="submit">Logout</button>
                        </form>
                    </li>
                <?php else: ?>
                    <li class="nav-item">
                        <a class="nav-link" href="../signin.php"><button class="btn btn-outline-primary">Sign In</button></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../signup.php"><button class="btn btn-outline-success">Sign Up</button></a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>