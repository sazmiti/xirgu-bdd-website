<?php
session_start();
if (!isset($_SESSION['id'])) {
    http_response_code(401);
    die('Forbidden');
} else {
    if (!($_SESSION['role'] == "admin")) {
        http_response_code(403);
        die('Forbidden');
    }
}

$resource = htmlspecialchars($_GET["resource"]);
$lang = htmlspecialchars($_GET["lang"]);
if (isset($_GET["type"]))
    $type = htmlspecialchars($_GET["type"]);
if (isset($_GET["country"]))
    $country = htmlspecialchars($_GET["country"]);
if (isset($_GET["author"]))
    $author = htmlspecialchars($_GET["author"]);
$pageParameters = "";
if(!empty($type)) { $pageParameters = "&type={$_GET['type']}"; }
if(!empty($country)) { $pageParameters = "{$pageParameters}&country={$_GET['country']}"; }
if(!empty($author)) { $pageParameters = "{$pageParameters}&author={$_GET['author']}"; }
$pageParameters = preg_replace("/&/", "?", $pageParameters, 1);


if (empty($resource) || empty($lang)) {
    header("Location: ../index.php");
    exit();
} else {
    if ($lang == "en"){
        include_once "en_bdd.php";
        $query = $dbEN->prepare("DELETE FROM ressource WHERE id = '". $resource ."';");
        $query->execute();
        header("Location: ../en/edit_resource.php". $pageParameters);
        exit();
    }elseif ($lang == "es"){
        include_once "esp_bdd.php";
        $query = $dbESP->prepare("DELETE FROM ressource WHERE id = '". $resource ."';");
        $query->execute();
        header("Location: ../esp/edit_resource.php". $pageParameters);
        exit();
    }else{
        header("Location: ../index.php");
        exit();
    }

}
