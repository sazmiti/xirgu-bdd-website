<?php
include_once "configDB.php";
try {
    $dbEN = new PDO(
        $settingsEN["driver"].":host=". $settingsEN["host"] .";port=". $settingsEN["port"] .";dbname=".$settingsEN["dbname"],
        $settingsEN["user"],
        $settingsEN["password"]
    );
}catch (PDOException $e){
    echo "Erreur : ". $e->getMessage();
    die();
}