<?php

// Activation de l'extention pgsql et pdo_pgsql dans php.ini
$settingsEN = array(
    "driver" => "pgsql",
    "host" => "localhost",
    "port" => "5432",
    "dbname" => "xirguEN",
    "user" => "postgres",
    "password" => "####"
);

$settingsESP = array(
    "driver" => "pgsql",
    "host" => "localhost",
    "port" => "5432",
    "dbname" => "xirguESP",
    "user" => "postgres",
    "password" => "####"
);

$settingsAccounts = array(
    "driver" => "pgsql",
    "host" => "localhost",
    "port" => "5432",
    "dbname" => "Accounts",
    "user" => "postgres",
    "password" => "####"
);


$settingsData = array(
    "dataPerPage" => 20
);