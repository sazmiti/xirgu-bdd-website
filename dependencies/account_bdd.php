<?php
include_once "configDB.php";
try {
    $dbAccounts = new PDO(
        $settingsAccounts["driver"].":host=". $settingsAccounts["host"] .";port=". $settingsAccounts["port"] .";dbname=".$settingsAccounts["dbname"],
        $settingsAccounts["user"],
        $settingsAccounts["password"]
    );
}catch (PDOException $e){
    echo "Erreur : ". $e->getMessage();
    die();
}