<nav class="navbar navbar-expand-lg navbar-light bg-light bg-white border-bottom shadow-sm">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Xirgu's Database</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <?php if(isset($_SESSION['id'])): ?>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/en/">English database</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/esp/">Spanish database</a>
                    </li>
                </ul>
            <?php endif; ?>
            <ul class="navbar-nav ms-auto">
                <?php if(isset($_SESSION['id'])): ?>
                    <li class="nav-item">
                        <form action="/dependencies/UsersManagement/logout.php" method="post" class="nav-link m-0">
                            <button class="btn btn-outline-danger" name="submit" type="submit">Logout</button>
                        </form>
                    </li>
                <?php else: ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/signin.php"><button class="btn btn-outline-primary">Sign In</button></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/signup.php"><button class="btn btn-outline-success">Sign Up</button></a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>