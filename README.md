Xirgu-BDD-Website
---- 
Welcome ! This databases were made by *Samuel Vannier* and *Thomas Pélissié de Montémont*.

Installation
----

**First, you need to have a PHP interpreter:**      
You can go by downloading php from : [PHP Download](https://www.php.net/downloads.php)   
Once you download PHP : 
- Go in the root folder of `PHP`
    - You will find `php.ini-development` and `php.ini-production`
    - Duplicate one of them depending on your situation and rename it `php.ini`
    - Modify `php.ini` :
        - Find the line with `extension_dir`
        - Modify the right one *(depends on your configuration)* and associate to the PHP `ext/` directory
        - Ex : `extension_dir ="c:/php/php7.4.0/ext/"`
        - And activate the `PostgreSQL driver`
        - By removing the semicolons in `;extension=pdo_pgsql` and `;extension=pgsql`


You can also use one of these : 
- Windows : [Wamp Server](https://www.wampserver.com/en/)
- Linux : [LAMP (Linux, Apache, MySQL, PHP)](http://doc.ubuntu-fr.org/lamp)
- MacOSX : [XAMP](https://sourceforge.net/projects/xampp/)

You will also need to activate the `PostgreSQL driver` by removing the semicolons in `php.ini` file from `;extension=pdo_pgsql` and `;extension=pgsql`.


**After you will need a to have a database:**      
We have use `PostgreSQL` so in the `database` folder you will find backups files of our database.
We got 2 database for **Xirgu's** data and one for the **users**. If you use `PostgreSQL` too, 
you just have to create database for each file and use the `Restore` button on PGAdmin.

**Once your database is setup:**
- Go to the `/dependencies/configDB.php` : 
  - Modify each setting with your configuration
  - You can also set the number of resources by page
    



