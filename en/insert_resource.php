<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Xirgu's database</title>
    <?php
    session_start();
    if (!isset($_SESSION['id'])){
        http_response_code(401);
        die('Forbidden');
    } else {
        if (!($_SESSION['role'] == "admin")){
            http_response_code(403);
            die('Forbidden');
        }
    }
    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/css.php";
    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/en_bdd.php";

    $query = $dbEN->prepare("SELECT id, name FROM profile;");
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);
    $profiles = [];
    foreach($query as $item) {
        $profiles[$item['id']] = $item['name'];
    }

    $query = $dbEN->prepare("SELECT * FROM location;");
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);
    $locations = [];
    foreach($query as $item) {
        $locations[$item['id']] = "{$item['continent']}, {$item['country']}, {$item['city']}, {$item['place']}";
    }
    ?>
</head>

<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/nav_en.php"?>

<div class="container">
    <div>
        <div id="accordion">
            <div class="card mt-3">
                <form class="card-body" method="post" action="insert.php">
                    <h5>
                        Informations :
                    </h5>
                    <div class="d-flex flex-wrap mb-3">
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">id</span>
                                <input name="re_id" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">title</span>
                                <input name="re_title" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">subtitle</span>
                                <input name="re_subtitle" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">type</span>
                                <input name="re_type" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">ressource_date</span>
                                <input name="re_ressource_date" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">ressource_date_inter</span>
                                <input name="re_ressource_date_inter" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">subject</span>
                                <input name="re_subject" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">description</span>
                                <input name="re_description" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">summary</span>
                                <input name="re_summary" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">lang</span>
                                <input name="re_lang" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">geographical_context</span>
                                <select name="re_geographical_context" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($locations as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <h5>
                        Contributors :
                    </h5>
                    <div class="d-flex flex-wrap mb-3">
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">author</span>
                                <select name="c_author" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($profiles as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">editor</span>
                                <select name="c_editor" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($profiles as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">publisher</span>
                                <select name="c_publisher" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($profiles as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">responsible_scientist</span>
                                <select name="c_responsible_scientist" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                <?php foreach ($profiles as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <h5>
                        Scientific Informations :
                    </h5>
                    <div class="d-flex flex-wrap mb-3">
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">notes</span>
                                <input name="s_notes" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">representation</span>
                                <input name="s_representation" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">document_location</span>
                                <select name="s_document_location" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($locations as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">expedition_location</span>
                                <select name="s_expedition_location" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($locations as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">receiver</span>
                                <select name="s_receiver" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($profiles as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <h5>
                        Rights :
                    </h5>
                    <div class="d-flex flex-wrap mb-3">
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">type</span>
                                <input name="ri_type" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">holders</span>
                                <select name="ri_holders" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($profiles as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <h5>
                        Analysis :
                    </h5>
                    <div class="d-flex flex-wrap mb-3">
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">creation_date</span>
                                <input name="a_creation_date" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">analysis_date</span>
                                <input name="a_analysis_date" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">revision_date</span>
                                <input name="a_revision_date" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">author</span>
                                <select name="a_author" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($profiles as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">revision_author</span>
                                <select name="a_revision_author" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($profiles as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">description_author</span>
                                <select name="a_description_author" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($profiles as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">transcript_author</span>
                                <select name="a_transcript_author" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($profiles as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <h5>
                        Genetic :
                    </h5>
                    <div class="d-flex flex-wrap mb-3">
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">state</span>
                                <input name="g_state" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">relation</span>
                                <input name="g_relation" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                    </div>
                    <h5>
                        Publication :
                    </h5>
                    <div class="d-flex flex-wrap mb-3">
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">title</span>
                                <input name="p_title" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">type</span>
                                <input name="p_type" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">periodicity</span>
                                <input name="p_periodicity" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">pnumber</span>
                                <input name="p_pnumber" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">location</span>
                                <select name="p_location" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($locations as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">director</span>
                                <select name="p_director" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                    <?php foreach ($profiles as $key => $value) : ?>
                                        <option value=<?= $key ?>><?= $value ?></option>
                                    <?php endforeach; ?>
                                    <option selected value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <h5>
                        Data :
                    </h5>
                    <div class="d-flex flex-wrap mb-3">
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">type</span>
                                <input name="d_type" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">support</span>
                                <input name="d_support" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">state</span>
                                <input name="d_state" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">x</span>
                                <input name="d_x" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">y</span>
                                <input name="d_y" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                    </div>
                    <h5>
                        File :
                    </h5>
                    <div class="d-flex flex-wrap mb-3">
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">name</span>
                                <input name="f_name" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">extension</span>
                                <input name="f_extension" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">ko_size</span>
                                <input name="f_ko_size" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">url</span>
                                <input name="f_url" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-3 w-auto mx-3 ">
                                <span class="input-group-text" id="basic-addon1" style="width: fit-content">last_consultation</span>
                                <input name="f_last_consultation" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="" style="width: fit-content">
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap justify-content-end">
                        <button type="submit" class="btn btn-primary">Insert</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>