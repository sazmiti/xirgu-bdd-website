<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/css.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/en_bdd.php";
session_start();
if (!isset($_SESSION['id'])){
    http_response_code(401);
    die('Forbidden');
} else {
    if (!($_SESSION['role'] == "admin")){
        http_response_code(403);
        die('Forbidden');
    }
}
    if(isset($_POST['re_id'])) {
        if($_POST['c_author'] || $_POST['c_editor'] || $_POST['c_publisher'] || $_POST['c_responsible_scientist']) {
            $conditions = "WHERE ";
            if($_POST['c_author']) $conditions = "{$conditions}author={$_POST['c_author']} AND ";
            else {
                $conditions = "{$conditions}author IS NULL AND ";
                $_POST['c_author']="NULL";
            }
            if($_POST['c_editor']) $conditions = "{$conditions}editor={$_POST['c_editor']} AND ";
            else {
                $conditions = "{$conditions}editor IS NULL AND ";
                $_POST['c_editor']="NULL";
            }
            if($_POST['c_publisher']) $conditions = "{$conditions}publisher={$_POST['c_publisher']} AND ";
            else {
                $conditions = "{$conditions}publisher IS NULL AND ";
                $_POST['c_publisher']="NULL";
            }
            if($_POST['c_responsible_scientist']) $conditions = "{$conditions}responsible_scientist={$_POST['c_responsible_scientist']}";
            else {
                $conditions = "{$conditions}responsible_scientist IS NULL";
                $_POST['c_responsible_scientist']="NULL";
            }

            $query = $dbEN->prepare("SELECT id FROM contributors {$conditions} LIMIT 1;");
            $query->execute();
            $c = $query->fetch(PDO::FETCH_ASSOC);
            
            if(isset($c['id'])) {
                $c = $c['id'];
            } else {
                $query = $dbEN->prepare("INSERT INTO contributors(author, editor, publisher, responsible_scientist) VALUES({$_POST['c_author']}, {$_POST['c_editor']}, {$_POST['c_publisher']}, {$_POST['c_responsible_scientist']});");
                $query->execute();
                $query = $dbEN->prepare("SELECT id FROM contributors {$conditions};");
                $query->execute();
                $c = $query->fetch(PDO::FETCH_ASSOC);
                $c = $c['id'];
            } 
        } else $c = "NULL";


        if($_POST['ri_type'] || $_POST['ri_holders']) {
            $conditions = "WHERE ";
            if($_POST['ri_type']) {
                $conditions = "{$conditions}type='{$_POST['ri_type']}' AND ";
                $_POST['ri_type']="'{$_POST['ri_type']}'";
            }
            else {
                $conditions = "{$conditions}type IS NULL AND ";
                $_POST['ri_type']="NULL";
            }
            if($_POST['ri_holders']) $conditions = "{$conditions}holders={$_POST['ri_holders']}";
            else {
                $conditions = "{$conditions}holders IS NULL";
                $_POST['ri_holders']="NULL";
            }

            $query = $dbEN->prepare("SELECT id FROM rights {$conditions} LIMIT 1;");
            $query->execute();
            $ri = $query->fetch(PDO::FETCH_ASSOC);
            
            if(isset($ri['id'])) {
                $ri = $ri['id'];
            } else {
                $query = $dbEN->prepare("INSERT INTO rights(type, holders) VALUES({$_POST['ri_type']}, {$_POST['ri_holders']});");
                $query->execute();
                $query = $dbEN->prepare("SELECT id FROM rights {$conditions} LIMIT 1;");
                $query->execute();
                $ri = $query->fetch(PDO::FETCH_ASSOC);
                $ri = $ri['id'];
            } 
        } else $ri = "NULL";


        if($_POST['a_creation_date'] || $_POST['a_analysis_date'] || $_POST['a_revision_date'] || $_POST['a_author'] || $_POST['a_revision_author'] || $_POST['a_description_author'] || $_POST['a_transcript_author']) {
            $conditions = "WHERE ";
            if($_POST['a_creation_date']) {
                $conditions = "{$conditions}creation_date=date'{$_POST['a_creation_date']}' AND ";
                $_POST['a_creation_date']="date'{$_POST['a_creation_date']}'";
            }
            else {
                $conditions = "{$conditions}creation_date IS NULL AND ";
                $_POST['a_creation_date']="NULL";
            }
            if($_POST['a_analysis_date']) {
                $conditions = "{$conditions}analysis_date=date'{$_POST['a_analysis_date']}' AND ";
                $_POST['a_analysis_date']="date'{$_POST['a_analysis_date']}'";
            }
            else {
                $conditions = "{$conditions}analysis_date IS NULL AND ";
                $_POST['a_analysis_date']="NULL";
            }
            if($_POST['a_revision_date']) {
                $conditions = "{$conditions}revision_date=date'{$_POST['a_revision_date']}' AND ";
                $_POST['a_revision_date']="date'{$_POST['a_revision_date']}'";
            }
            else {
                $conditions = "{$conditions}revision_date IS NULL AND ";
                $_POST['a_revision_date']="NULL";
            }
            if($_POST['a_author']) $conditions = "{$conditions}author={$_POST['a_author']} AND ";
            else {
                $conditions = "{$conditions}author IS NULL AND ";
                $_POST['a_author']="NULL";
            }
            if($_POST['a_revision_author']) $conditions = "{$conditions}revision_author={$_POST['a_revision_author']} AND ";
            else {
                $conditions = "{$conditions}revision_author IS NULL AND ";
                $_POST['a_revision_author']="NULL";
            }
            if($_POST['a_description_author']) $conditions = "{$conditions}description_author={$_POST['a_description_author']} AND ";
            else {
                $conditions = "{$conditions}description_author IS NULL AND ";
                $_POST['a_description_author']="NULL";
            }
            if($_POST['a_transcript_author']) $conditions = "{$conditions}transcript_author={$_POST['a_transcript_author']}";
            else {
                $conditions = "{$conditions}transcript_author IS NULL";
                $_POST['a_transcript_author']="NULL";
            }

            $query = $dbEN->prepare("SELECT id FROM analysis {$conditions} LIMIT 1;");
            $query->execute();
            $a = $query->fetch(PDO::FETCH_ASSOC);
            
            if(isset($a['id'])) {
                $a = $a['id'];
            } else {
                $query = $dbEN->prepare("INSERT INTO analysis(creation_date, analysis_date, revision_date, author, revision_author, description_author, transcript_author) VALUES({$_POST['a_creation_date']}, {$_POST['a_analysis_date']}, {$_POST['a_revision_date']}, {$_POST['a_author']}, {$_POST['a_revision_author']}, {$_POST['a_description_author']}, {$_POST['a_transcript_author']});");
                $query->execute();
                $query = $dbEN->prepare("SELECT id FROM analysis {$conditions} LIMIT 1;");
                $query->execute();
                $a = $query->fetch(PDO::FETCH_ASSOC);
                $a = $a['id'];
            }
        } else $a = "NULL";


        if($_POST['g_state'] || $_POST['g_relation']) {
            $conditions = "WHERE ";
            if($_POST['g_state']) {
                $conditions = "{$conditions}state='{$_POST['g_state']}' AND ";
                $_POST['g_state']="'{$_POST['g_state']}'";
            }
            else {
                $conditions = "{$conditions}state IS NULL AND ";
                $_POST['g_state']="NULL";
            }
            if($_POST['g_relation']) {
                $conditions = "{$conditions}relation='{$_POST['g_relation']}'";
                $_POST['g_relation']="'{$_POST['g_relation']}'";
            }
            else {
                $conditions = "{$conditions}relation IS NULL";
                $_POST['g_relation']="NULL";
            }

            $query = $dbEN->prepare("SELECT id FROM genetic {$conditions} LIMIT 1;");
            $query->execute();
            $g = $query->fetch(PDO::FETCH_ASSOC);
            
            if(isset($g['id'])) {
                $g = $g['id'];
            } else {
                $query = $dbEN->prepare("INSERT INTO genetic(state, relation) VALUES({$_POST['g_state']}, {$_POST['g_relation']});");
                $query->execute();
                $query = $dbEN->prepare("SELECT id FROM genetic {$conditions} LIMIT 1;");
                $query->execute();
                $g = $query->fetch(PDO::FETCH_ASSOC);
                $g = $g['id'];
            } 
        } else $g = "NULL";


        if($_POST['p_title'] || $_POST['p_type'] || $_POST['p_periodicity'] || $_POST['p_pnumber'] || $_POST['p_location'] || $_POST['p_director']) {
            $conditions = "WHERE ";
            if($_POST['p_title']) {
                $conditions = "{$conditions}title='{$_POST['p_title']}' AND ";
                $_POST['p_title']="'{$_POST['p_title']}'";
            }
            else {
                $conditions = "{$conditions}title IS NULL AND ";
                $_POST['p_title']="NULL";
            }
            if($_POST['p_type']) {
                $conditions = "{$conditions}type='{$_POST['p_type']}' AND ";
                $_POST['p_type']="'{$_POST['p_type']}'";
            }
            else {
                $conditions = "{$conditions}type IS NULL AND ";
                $_POST['p_type']="NULL";
            }
            if($_POST['p_periodicity']) {
                $conditions = "{$conditions}periodicity='{$_POST['p_periodicity']}' AND ";
                $_POST['p_periodicity']="'{$_POST['p_periodicity']}'";
            }
            else {
                $conditions = "{$conditions}periodicity IS NULL AND ";
                $_POST['p_periodicity']="NULL";
            }
            if($_POST['p_pnumber']) $conditions = "{$conditions}pnumber={$_POST['p_pnumber']} AND ";
            else {
                $conditions = "{$conditions}pnumber IS NULL AND ";
                $_POST['p_pnumber']="NULL";
            }
            if($_POST['p_location']) $conditions = "{$conditions}location={$_POST['p_location']} AND ";
            else {
                $conditions = "{$conditions}location IS NULL AND ";
                $_POST['p_location']="NULL";
            }
            if($_POST['p_director']) $conditions = "{$conditions}director={$_POST['p_director']}";
            else {
                $conditions = "{$conditions}director IS NULL";
                $_POST['p_director']="NULL";
            }
            
            $query = $dbEN->prepare("SELECT id FROM publication {$conditions} LIMIT 1;");
            $query->execute();
            $p = $query->fetch(PDO::FETCH_ASSOC);
            
            if(isset($p['id'])) {
                $p = $p['id'];
            } else {
                $query = $dbEN->prepare("INSERT INTO publication(title, type, periodicity, pnumber, location, director) VALUES({$_POST['p_title']}, {$_POST['p_type']}, {$_POST['p_periodicity']}, {$_POST['p_pnumber']}, {$_POST['p_location']}, {$_POST['p_director']});");
                $query->execute();
                $query = $dbEN->prepare("SELECT id FROM publication {$conditions} LIMIT 1;");
                $query->execute();
                $p = $query->fetch(PDO::FETCH_ASSOC);
                $p = $p['id'];
            }
        } else $p = "NULL";


        if($_POST['s_notes'] || $_POST['s_representation'] || $_POST['s_document_location'] || $_POST['s_expedition_location'] || $_POST['s_receiver'] || $ri!="NULL" || $a!="NULL" || $g!="NULL" || $p!="NULL") {
            $conditions = "WHERE ";
            if($_POST['s_notes']) {
                $conditions = "{$conditions}notes='{$_POST['s_notes']}' AND ";
                $_POST['s_notes']="'{$_POST['s_notes']}'";
            }
            else {
                $conditions = "{$conditions}notes IS NULL AND ";
                $_POST['s_notes']="NULL";
            }
            if($_POST['s_representation']) {
                $conditions = "{$conditions}representation='{$_POST['s_representation']}' AND ";
                $_POST['s_representation']="'{$_POST['s_representation']}'";
            }
            else {
                $conditions = "{$conditions}representation IS NULL AND ";
                $_POST['s_representation']="NULL";
            }
            if($_POST['s_document_location']) $conditions = "{$conditions}document_location={$_POST['s_document_location']} AND ";
            else {
                $conditions = "{$conditions}document_location IS NULL AND ";
                $_POST['s_document_location']="NULL";
            }
            if($_POST['s_expedition_location']) $conditions = "{$conditions}expedition_location={$_POST['s_expedition_location']} AND ";
            else {
                $conditions = "{$conditions}expedition_location IS NULL AND ";
                $_POST['s_expedition_location']="NULL";
            }
            if($_POST['s_receiver']) $conditions = "{$conditions}receiver={$_POST['s_receiver']} AND ";
            else {
                $conditions = "{$conditions}receiver IS NULL AND ";
                $_POST['s_receiver']="NULL";
            }
            if($ri!="NULL" && $ri!=null) $conditions = "{$conditions}rights={$ri} AND ";
            else {
                $conditions = "{$conditions}rights IS NULL AND ";
            }
            if($a!="NULL" && $a!=null) $conditions = "{$conditions}analysis={$a} AND ";
            else {
                $conditions = "{$conditions}analysis IS NULL AND ";
            }
            if($g!="NULL" && $g!=null) $conditions = "{$conditions}genetic={$g} AND ";
            else {
                $conditions = "{$conditions}genetic IS NULL AND ";
            }
            if($p!="NULL" && $p!=null) $conditions = "{$conditions}publication={$p}";
            else {
                $conditions = "{$conditions}publication IS NULL";
            }

            $query = $dbEN->prepare("SELECT id FROM scientificinfo {$conditions} LIMIT 1;");
            $query->execute();
            $s = $query->fetch(PDO::FETCH_ASSOC);

            if(isset($s['id'])) {
                $s = $s['id'];
            } else {
                $query = $dbEN->prepare("INSERT INTO scientificinfo(notes, representation, document_location, expedition_location, receiver, rights, analysis, genetic, publication) VALUES({$_POST['s_notes']}, {$_POST['s_representation']}, {$_POST['s_document_location']}, {$_POST['s_expedition_location']}, {$_POST['s_receiver']}, {$ri}, {$a}, {$g}, {$p});");
                $query->execute();
                $query = $dbEN->prepare("SELECT id FROM scientificinfo {$conditions} LIMIT 1;");
                $query->execute();
                $s = $query->fetch(PDO::FETCH_ASSOC);
                $s = $s['id'];
            }
        } else $s = "NULL";


        if($_POST['f_name'] || $_POST['f_extension'] || $_POST['f_url'] || $_POST['f_ko_size'] || $_POST['f_last_consultation']) {
            $conditions = "WHERE ";
            if($_POST['f_name']) {
                $conditions = "{$conditions}name='{$_POST['f_name']}' AND ";
                $_POST['f_name']="'{$_POST['f_name']}'";
            }
            else {
                $conditions = "{$conditions}name IS NULL AND ";
                $_POST['f_name']="NULL";
            }
            if($_POST['f_extension']) {
                $conditions = "{$conditions}extension='{$_POST['f_extension']}' AND ";
                $_POST['f_extension']="'{$_POST['f_extension']}'";
            }
            else {
                $conditions = "{$conditions}extension IS NULL AND ";
                $_POST['f_extension']="NULL";
            }
            if($_POST['f_url']) {
                $conditions = "{$conditions}url='{$_POST['f_url']}' AND ";
                $_POST['f_url']="'{$_POST['f_url']}'";
            }
            else {
                $conditions = "{$conditions}url IS NULL AND ";
                $_POST['f_url']="NULL";
            }
            if($_POST['f_ko_size']) $conditions = "{$conditions}ko_size={$_POST['f_ko_size']} AND ";
            else {
                $conditions = "{$conditions}ko_size IS NULL AND ";
                $_POST['f_ko_size']="NULL";
            }
            if($_POST['f_last_consultation']) {
                $conditions = "{$conditions}last_consultation=date'{$_POST['f_last_consultation']}'";
                $_POST['f_last_consultation']="date'{$_POST['f_last_consultation']}'";
            }
            else {
                $conditions = "{$conditions}last_consultation IS NULL";
                $_POST['f_last_consultation']="NULL";
            }

            $query = $dbEN->prepare("SELECT id FROM file WHERE id='{$_POST['re_id']}';");
            $query->execute();
            $f = $query->fetch(PDO::FETCH_ASSOC);
            
            if(isset($f['id'])) {
                $query = $dbEN->prepare("UPDATE file SET name={$_POST['f_name']}, extension={$_POST['f_extension']}, url={$_POST['f_url']}, ko_size={$_POST['f_ko_size']}, last_consultation={$_POST['f_last_consultation']} WHERE id='{$_POST['re_id']}';");
                $query->execute();
                $query = $dbEN->prepare("SELECT id FROM file WHERE id='{$_POST['re_id']}';");
                $query->execute();
                $f = $query->fetch(PDO::FETCH_ASSOC);
            } else {
                $query = $dbEN->prepare("INSERT INTO file(id, name, extension, url, ko_size, last_consultation) VALUES('{$_POST['re_id']}', {$_POST['f_name']}, {$_POST['f_extension']}, {$_POST['f_url']}, {$_POST['f_ko_size']}, {$_POST['f_last_consultation']});");
                $query->execute();
                $query = $dbEN->prepare("SELECT id FROM file WHERE id='{$_POST['re_id']}';");
                $query->execute();
                $f = $query->fetch(PDO::FETCH_ASSOC);
            }
            $f = "'{$f['id']}'";
        } else $f = "NULL";


        if($_POST['d_type'] || $_POST['d_support'] || $_POST['d_state'] || $_POST['d_x'] || $_POST['d_y'] || $f !="NULL") {
            $conditions = "WHERE ";
            if($_POST['d_type']) {
                $conditions = "{$conditions}type='{$_POST['d_type']}' AND ";
                $_POST['d_type']="'{$_POST['d_type']}'";
            }
            else {
                $conditions = "{$conditions}type IS NULL AND ";
                $_POST['d_type']="NULL";
            }
            if($_POST['d_support']) {
                $conditions = "{$conditions}support='{$_POST['d_support']}' AND ";
                $_POST['d_support']="'{$_POST['d_support']}'";
            }
            else {
                $conditions = "{$conditions}support IS NULL AND ";
                $_POST['d_support']="NULL";
            }
            if($_POST['d_state']) {
                $conditions = "{$conditions}state='{$_POST['d_state']}' AND ";
                $_POST['d_state']="'{$_POST['d_state']}'";
            }
            else {
                $conditions = "{$conditions}sate IS NULL AND ";
                $_POST['d_state']="NULL";
            }
            if($_POST['d_x']) $conditions = "{$conditions}x={$_POST['d_x']} AND ";
            else {
                $conditions = "{$conditions}x IS NULL AND ";
                $_POST['d_x']="NULL";
            }
            if($_POST['d_y']) $conditions = "{$conditions}y={$_POST['d_y']} AND ";
            else {
                $conditions = "{$conditions}y IS NULL AND ";
                $_POST['d_y']="NULL";
            }
            if($f != "NULL" && $f && $f=="") $conditions = "{$conditions}file={$f}";
            else {
                $conditions = "{$conditions}file IS NULL";
            }

            $query = $dbEN->prepare("SELECT id FROM data WHERE id='{$_POST['re_id']}';");
            $query->execute();
            $d = $query->fetch(PDO::FETCH_ASSOC);
            
            if(isset($d['id'])) {
                $query = $dbEN->prepare("UPDATE data SET type={$_POST['d_type']}, support={$_POST['d_support']}, state={$_POST['d_state']}, x={$_POST['d_x']}, y={$_POST['d_y']}, file={$f} WHERE id='{$_POST['re_id']}';");
                $query->execute();
                $query = $dbEN->prepare("SELECT id FROM data where id='{$_POST['re_id']}';");
                $query->execute();
                $d = $query->fetch(PDO::FETCH_ASSOC);
            } else {
                $query = $dbEN->prepare("INSERT INTO data(id, type, support, state, x, y, file) VALUES('{$_POST['re_id']}', {$_POST['d_type']}, {$_POST['d_support']}, {$_POST['d_state']}, {$_POST['d_x']}, {$_POST['d_y']}, {$f});");
                $query->execute();
                $query = $dbEN->prepare("SELECT id FROM data where id='{$_POST['re_id']}';");
                $query->execute();
                $d = $query->fetch(PDO::FETCH_ASSOC);
            }
            $d = "'{$d['id']}'";
        } else $d = "NULL";


        if($_POST['re_title']) {
            $_POST['re_title'] = "'{$_POST['re_title']}'";
        } else {
            $_POST['re_title'] = "NULL";
        }
        if($_POST['re_subtitle']) {
            $_POST['re_subtitle'] = "'{$_POST['re_subtitle']}'";
        } else {
            $_POST['re_subtitle'] = "NULL";
        }
        if($_POST['re_type']) {
            $_POST['re_type'] = "'{$_POST['re_type']}'";
        } else {
            $_POST['re_type'] = "NULL";
        }
        if($_POST['re_ressource_date']) {
            $_POST['re_ressource_date'] = "date'{$_POST['re_ressource_date']}'";
        } else {
            $_POST['re_ressource_date'] = "NULL";
        }
        if($_POST['re_ressource_date_inter']) {
            $_POST['re_ressource_date_inter'] = "date'{$_POST['re_ressource_date_inter']}'";
        } else {
            $_POST['re_ressource_date_inter'] = "NULL";
        }
        if($_POST['re_subject']) {
            $_POST['re_subject'] = "'{$_POST['re_subject']}'";
        } else {
            $_POST['re_subject'] = "NULL";
        }
        if($_POST['re_description']) {
            $_POST['re_description'] = "'{$_POST['re_description']}'";
        } else {
            $_POST['re_description'] = "NULL";
        }
        if($_POST['re_summary']) {
            $_POST['re_summary'] = "'{$_POST['re_summary']}'";
        } else {
            $_POST['re_summary'] = "NULL";
        }
        if($_POST['re_lang']) {
            $_POST['re_lang'] = "'{$_POST['re_lang']}'";
        } else {
            $_POST['re_lang'] = "NULL";
        }
        if($_POST['re_geographical_context']) {

        } else {
            $_POST['re_geographical_context'] = "NULL";
        }
        

        $query = $dbEN->prepare("SELECT id FROM ressource WHERE id='{$_POST['re_id']}';");
        $query->execute();
        $re = $query->fetch(PDO::FETCH_ASSOC);
        if(isset($re['id']) && $re['id'] && $re['id']!="") {
            $query = $dbEN->prepare("UPDATE ressource SET title={$_POST['re_title']}, subtitle={$_POST['re_subtitle']}, type={$_POST['re_type']}, 
            ressource_date={$_POST['re_ressource_date']}, ressource_date_inter={$_POST['re_ressource_date_inter']}, subject={$_POST['re_subject']}, 
            description={$_POST['re_description']}, summary={$_POST['re_summary']}, lang={$_POST['re_lang']}, geographical_context={$_POST['re_geographical_context']}, 
            contributors={$c}, scientific_info={$s}, data_info={$d} WHERE id='{$_POST['re_id']}';");
            $query->execute();
        }
        else {
            $query = $dbEN->prepare("INSERT INTO ressource(id, title, subtitle, type, ressource_date, ressource_date_inter, subject, description, summary, lang, geographical_context, contributors, scientific_info, data_info)
            VALUES('{$_POST['re_id']}', {$_POST['re_title']}, {$_POST['re_subtitle']}, {$_POST['re_type']}, {$_POST['re_ressource_date']}, {$_POST['re_ressource_date_inter']}, 
            {$_POST['re_subject']}, {$_POST['re_description']}, {$_POST['re_summary']}, {$_POST['re_lang']}, {$_POST['re_geographical_context']}, {$c}, {$s}, {$d});");
            $query->execute();
        }
    }
    header("Location: {$_SERVER['HTTP_REFERER']}");
?>