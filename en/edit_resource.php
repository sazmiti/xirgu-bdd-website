<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Xirgu's database</title>
    <?php
    session_start();
    if (!isset($_SESSION['id'])){
        http_response_code(401);
        die('Forbidden');
    } else {
        if (!($_SESSION['role'] == "admin")){
            http_response_code(403);
            die('Forbidden');
        }
    }
    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/css.php";
    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/en_bdd.php";

    if(isset($_GET['page']) && !empty($_GET['page'])){
        $currentPage = (int) strip_tags($_GET['page']);
    }else{
        $currentPage = 1;
    }


    $tables = "ressource r";
    $conditions = "";
    $pageparameters = "";

    if(isset($_GET['type'])) {
        $conditions = "WHERE r.type='{$_GET['type']}'";
        $pageparameters = "&type={$_GET['type']}";
    }

    if(isset($_GET['country'])) {
        $tables = $tables . ", location l";
        $pageparameters = "{$pageparameters}&country={$_GET['country']}";
        if(strlen($conditions)>0) $conditions = "{$conditions} AND r.geographical_context = l.id AND l.country='{$_GET['country']}'";
        else $conditions = "WHERE r.geographical_context=l.id AND l.country='{$_GET['country']}'";
    }

    if(isset($_GET['author'])) {
        $tables = $tables . ", contributors c";
        $pageparameters = "{$pageparameters}&author={$_GET['author']}";
        if(strlen($conditions)>0) $conditions = "{$conditions} AND r.contributors = c.id AND c.author={$_GET['author']}";
        else $conditions = "WHERE r.contributors=c.id AND c.author=" . $_GET['author'];
    }
    

    $query = $dbEN->prepare("SELECT COUNT(*) as resource_nb FROM {$tables} {$conditions};");
    $query->execute();
    $resourceNb = $query->fetch();
    $resourceNb = $resourceNb['resource_nb'];

    $pages = ceil($resourceNb/$settingsData['dataPerPage']);
    $firstResource = (($currentPage - 1)*$settingsData['dataPerPage']);

    $query = $dbEN->prepare("SELECT r.* FROM {$tables} {$conditions} ORDER BY substring(r.id from 6)::integer ASC LIMIT {$settingsData['dataPerPage']} OFFSET {$firstResource};");
    $query->execute();
    $resources = $query->fetchAll(PDO::FETCH_ASSOC);


    $query = $dbEN->prepare("SELECT DISTINCT country FROM location WHERE country IS NOT NULL;");
    $query->execute();
    $countries = $query->fetchAll(PDO::FETCH_ASSOC);

    $query = $dbEN->prepare("SELECT DISTINCT type FROM ressource WHERE type IS NOT NULL;");
    $query->execute();
    $types = $query->fetchAll(PDO::FETCH_ASSOC);

    $query = $dbEN->prepare("SELECT DISTINCT p.id, p.name FROM profile p, contributors c WHERE c.author = p.id;");
    $query->execute();
    $authors = $query->fetchAll(PDO::FETCH_ASSOC);
    

    $query = $dbEN->prepare("SELECT id, name FROM profile;");
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);
    $profiles = [];
    foreach($query as $item) {
        $profiles[$item['id']] = $item['name'];
    }

    $query = $dbEN->prepare("SELECT * FROM location;");
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);
    $locations = [];
    foreach($query as $item) {
        $locations[$item['id']] = "{$item['continent']}, {$item['country']}, {$item['city']}, {$item['place']}";
    }


    $contributors = [0 => ['author' => null, 'editor' => null, 'publisher' => null, 'responsible_scientist' => null]];
    $scientificinfos = [0 => ['notes' => null, 'representation' => null, 'receiver' => null, 'document_location' => null, 'expedition_location' => null, 'rights' => 0, 'genetic' => 0, 'analysis' => 0, 'publication' => 0]];
    $rights = [0 => ['type' => null, 'holders' => null]];
    $analysis = [0 => ['creation_date' => null, 'analysis_date' => null, 'revision_date' => null, 'author' => null, 'revision_author' => null, 'description_author' => null, 'transcript_author' => null]];
    $genetics = [0 => ['state' => null, 'relation' => null]];
    $publications = [0 => ['title' => null, 'type' => null, 'periodicity' => null, 'pnumber' => null, 'location' => null, 'director' => null]];

    
    for ($i=0; $i<sizeof($resources); $i++) {
        if($resources[$i]["geographical_context"]) $resources[$i]["geographical_context"] = $locations[$resources[$i]["geographical_context"]];
        if($resources[$i]['contributors']) {
            if(!key_exists($resources[$i]['contributors'], $contributors)) {
                $query = $dbEN->prepare("SELECT author, editor, publisher, responsible_scientist FROM contributors WHERE id={$resources[$i]['contributors']};");
                $query->execute();
                $query = $query->fetch();
                
                $contributors[$resources[$i]['contributors']] = [];
                
                if($query['author']) $contributors[$resources[$i]['contributors']]['author'] = $profiles[$query['author']];
                else $contributors[$resources[$i]['contributors']]['author'] = null;
                if($query['editor']) $contributors[$resources[$i]['contributors']]['editor'] = $profiles[$query['editor']];
                else $contributors[$resources[$i]['contributors']]['editor'] = null;
                if($query['publisher']) $contributors[$resources[$i]['contributors']]['publisher'] = $profiles[$query['publisher']];
                else $contributors[$resources[$i]['contributors']]['publisher'] = null;
                if($query['responsible_scientist']) $contributors[$resources[$i]['contributors']]['responsible_scientist'] = $profiles[$query['responsible_scientist']];
                else $contributors[$resources[$i]['contributors']]['responsible_scientist'] = null;
            }
        } else $resources[$i]['contributors'] = 0;

        if($resources[$i]['data_info']) {
            $query = $dbEN->prepare("SELECT type, support, state, x, y, file FROM data WHERE id='{$resources[$i]['data_info']}';");
            $query->execute();
            $resources[$i]['data'] = $query->fetch();

            if($resources[$i]['data']['file']) {
                $query = $dbEN->prepare("SELECT name, extension, ko_size, url, last_consultation FROM file WHERE id='{$resources[$i]['data']['file']}';");
                $query->execute();
                $resources[$i]['file'] = $query->fetch();
            } else $resources[$i]['file'] = ['name' => null, 'extension' => null, 'ko_size' => null, 'url' => null, 'last_consultation' => null];
        } else {
            $resources[$i]['data'] = ['type' => null, 'support' => null, 'state' => null, 'x' => null, 'y' => null];
            $resources[$i]['file'] = ['name' => null, 'extension' => null, 'ko_size' => null, 'url' => null, 'last_consultation' => null];
        }

        

        if($resources[$i]['scientific_info'] && $resources[$i]['scientific_info'] !="") {
            if(!key_exists($resources[$i]['scientific_info'], $scientificinfos)) {
                $query = $dbEN->prepare("SELECT * FROM scientificinfo WHERE id={$resources[$i]['scientific_info']};");
                $query->execute();
                $query = $query->fetch();
                
                $scientificinfos[$resources[$i]['scientific_info']] = ['notes' => $query['notes'], 'representation' => $query['representation']];
                
                if($query['receiver']) $scientificinfos[$resources[$i]['scientific_info']]['receiver'] = $profiles[$query['receiver']];
                else $scientificinfos[$resources[$i]['scientific_info']]['receiver'] = null;
                if($query['document_location']) $scientificinfos[$resources[$i]['scientific_info']]['document_location'] = $locations[$query['document_location']];
                else $scientificinfos[$resources[$i]['scientific_info']]['document_location'] = null;
                if($query['expedition_location']) $scientificinfos[$resources[$i]['scientific_info']]['expedition_location'] = $locations[$query['expedition_location']];
                else $scientificinfos[$resources[$i]['scientific_info']]['expedition_location'] = null;
                
                
                if($query['rights']) {
                    $scientificinfos[$resources[$i]['scientific_info']]['rights'] = $query['rights'];
                    if(!key_exists($query['rights'], $rights)) {
                        $subquery = $dbEN->prepare("SELECT * FROM rights WHERE id={$query['rights']};");
                        $subquery->execute();
                        $subquery = $subquery->fetch();
                        $rights[$query['rights']] = array('type' => $subquery['type']);
                        if($subquery['holders']) $rights[$query['rights']]['holders'] = $profiles[$subquery['holders']];
                        else $rights[$query['rights']]['holders'] = null;
                    }
                } else $scientificinfos[$resources[$i]['scientific_info']]['rights'] = 0;

                if($query['genetic']) {
                    $scientificinfos[$resources[$i]['scientific_info']]['genetic'] = $query['genetic'];
                    if(!key_exists($query['genetic'], $genetics)) {
                        $subquery = $dbEN->prepare("SELECT * FROM genetic WHERE id={$query['genetic']};");
                        $subquery->execute();
                        $subquery = $subquery->fetch();
                        $genetics[$query['genetic']] = array('state' => $subquery['state'], 'relation' => $subquery['relation']);
                    }
                } else $scientificinfos[$resources[$i]['scientific_info']]['genetic'] = 0;

                if($query['analysis']) {
                    $scientificinfos[$resources[$i]['scientific_info']]['analysis'] = $query['analysis'];
                    if(!key_exists($query['analysis'], $analysis)) {
                        $subquery = $dbEN->prepare("SELECT * FROM analysis WHERE id={$query['analysis']};");
                        $subquery->execute();
                        $subquery = $subquery->fetch();

                        $analysis[$query['analysis']] = ['creation_date' => $subquery['creation_date'], 'analysis_date' => $subquery['analysis_date'], 'revision_date' => $subquery['revision_date']];
                        if(isset($subquery['author']))$analysis[$query['analysis']]['author'] = $profiles[$subquery['author']];
                        else $analysis[$query['analysis']]['author'] = null;
                        if(isset($subquery['revision_author']))$analysis[$query['analysis']]['revision_author'] = $profiles[$subquery['revision_author']];
                        else $analysis[$query['analysis']]['revision_author'] = null;
                        if(isset($subquery['description_author']))$analysis[$query['analysis']]['description_author'] = $profiles[$subquery['description_author']];
                        else $analysis[$query['analysis']]['description_author'] = null;
                        if(isset($subquery['transcript_author']))$analysis[$query['analysis']]['transcript_author'] = $profiles[$subquery['transcript_author']];
                        else $analysis[$query['analysis']]['transcript_author'] = null;
                    }
                    
                } else $scientificinfos[$resources[$i]['scientific_info']]['analysis'] = 0;

                if($query['publication']) {
                    $scientificinfos[$resources[$i]['scientific_info']]['publication'] = $query['publication'];
                    if(!key_exists($query['publication'], $publications)) {
                        $subquery = $dbEN->prepare("SELECT * FROM publication WHERE id={$query['publication']};");
                        $subquery->execute();
                        $subquery = $subquery->fetch();
        
                        $publications[$query['publication']] = array('title' => $subquery['title'], 'type' => $subquery['type'], 'periodicity' => $subquery['periodicity'], 'pnumber' => $subquery['pnumber']);
                        if(isset($subquery['location']))$publications[$query['publication']]['location'] = $locations[$subquery['location']];
                        else $publications[$query['publication']]['location'] = null;
                        if(isset($subquery['director']))$publications[$query['publication']]['director'] = $profiles[$subquery['director']];
                        else $publications[$query['publication']]['director'] = null;
                    }
                } else $scientificinfos[$resources[$i]['scientific_info']]['publication'] = 0;
            }
        } else $resources[$i]['scientific_info'] = 0;
    }
    ?>
</head>

<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/nav_en.php"?>

<div class="container">
    <div class="d-flex justify-content-center mt-3">
        <form class="row gy-2 gx-3 align-items-center mb-0"> 
            <div class="col-auto">
                <label class="visually-hidden" for="autoSizingSelect">Preference</label>
                <select name="type" class="form-select" id="autoSizingSelect">
                    <option <?php if(!isset($_GET['type'])) echo "selected";?> disabled hidden>Type</option>
                    <?php foreach ($types as $item) : ?>
                        <option <?php if(isset($_GET['type'])) if($_GET['type']==$item['type']) echo "selected";?> value="<?= $item['type'] ?>"><?= $item['type'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-auto">
                <label class="visually-hidden" for="autoSizingS elect">Preference</label>
                <select name="country" class="form-select" id="autoSizingSelect" value="Spain">
                    <option <?php if(!isset($_GET['country'])) echo "selected";?> disabled hidden>Country</option>
                    <?php foreach ($countries as $item) : ?>
                        <option <?php if(isset($_GET['country'])) if($_GET['country']==$item['country']) echo "selected";?> value="<?= $item['country'] ?>"><?= $item['country'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-auto">
                <label class="visually-hidden" for="autoSizingSelect">Preference</label>
                <select name="author" class="form-select" id="autoSizingSelect">
                    <option <?php if(!isset($_GET['author'])) echo "selected";?> disabled hidden>Author</option>
                    <?php foreach ($authors as $item) : ?>
                        <option <?php if(isset($_GET['author'])) if($_GET['author']==$item['id']) echo "selected";?> value="<?= $item['id'] ?>"><?= $item['name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary">Filter</button>
            </div>
        </form>
    </div>
    <div>
        <div id="accordion">
            <?php for ($i=0; $i<sizeof($resources); $i++):?>
                <div class="card mt-3">
                    <div class="card-header" id="'. $resources[$i]["id"] .'">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#<?= $resources[$i]["id"] ?>collaspe" aria-expanded="false" aria-controls="<?= $resources[$i]["id"] ?>collaspe">
                                <?= $resources[$i]["id"] . " | " . $resources[$i]["title"] . " | " . $resources[$i]["type"] ?>
                            </button>
                        </h5>
                    </div>

                    <div id="<?= $resources[$i]["id"] ?>collaspe" class="collapse" aria-labelledby="<?= $resources[$i]["id"] ?>" data-parent="#accordion">
                    <form class="card-body" method="post" action="insert.php">
                        <h5>
                            Informations :
                        </h5>
                        <div class="d-flex flex-wrap mb-3">
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">id</span>
                                    <input name="re_id" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["id"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">title</span>
                                    <input name="re_title" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["title"]?>"" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">subtitle</span>
                                    <input name="re_subtitle" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["subtitle"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">type</span>
                                    <input name="re_type" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["type"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">ressource_date</span>
                                    <input name="re_ressource_date" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["ressource_date"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">ressource_date_inter</span>
                                    <input name="re_ressource_date_inter" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["ressource_date_inter"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">subject</span>
                                    <input name="re_subject" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["subject"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">description</span>
                                    <input name="re_description" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["description"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">summary</span>
                                    <input name="re_summary" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["summary"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">lang</span>
                                    <input name="re_lang" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["lang"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">geographical_context</span>
                                    <select name="re_geographical_context" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($locations as $key => $value) : ?>
                                            <option <?php if($value == $resources[$i]["geographical_context"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$resources[$i]["geographical_context"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h5>
                            Contributors :
                        </h5>
                        <div class="d-flex flex-wrap mb-3">
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">author</span>
                                    <select name="c_author" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($profiles as $key => $value) : ?>
                                            <option <?php if($value == $contributors[$resources[$i]["contributors"]]["author"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$contributors[$resources[$i]["contributors"]]["author"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">editor</span>
                                    <select name="c_editor" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($profiles as $key => $value) : ?>
                                            <option <?php if($value == $contributors[$resources[$i]["contributors"]]["editor"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$contributors[$resources[$i]["contributors"]]["editor"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">publisher</span>
                                    <select name="c_publisher" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($profiles as $key => $value) : ?>
                                            <option <?php if($value == $contributors[$resources[$i]["contributors"]]["publisher"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$contributors[$resources[$i]["contributors"]]["publisher"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">responsible_scientist</span>
                                    <select name="c_responsible_scientist" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($profiles as $key => $value) : ?>
                                            <option <?php if($value == $contributors[$resources[$i]["contributors"]]["responsible_scientist"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$contributors[$resources[$i]["contributors"]]["responsible_scientist"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h5>
                            Scientific Informations :
                        </h5>
                        <div class="d-flex flex-wrap mb-3">
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">notes</span>
                                    <input name="s_notes" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$scientificinfos[$resources[$i]["scientific_info"]]["notes"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">representation</span>
                                    <input name="s_representation" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$scientificinfos[$resources[$i]["scientific_info"]]["representation"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">document_location</span>
                                    <select name="s_document_location" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($locations as $key => $value) : ?>
                                            <option <?php if($value == $scientificinfos[$resources[$i]["scientific_info"]]["document_location"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$scientificinfos[$resources[$i]["scientific_info"]]["document_location"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">expedition_location</span>
                                    <select name="s_expedition_location" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($locations as $key => $value) : ?>
                                            <option <?php if($value == $scientificinfos[$resources[$i]["scientific_info"]]["expedition_location"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$scientificinfos[$resources[$i]["scientific_info"]]["expedition_location"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">receiver</span>
                                    <select name="s_receiver" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($profiles as $key => $value) : ?>
                                            <option <?php if($value == $scientificinfos[$resources[$i]["scientific_info"]]["receiver"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$scientificinfos[$resources[$i]["scientific_info"]]["receiver"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h5>
                            Rights :
                        </h5>
                        <div class="d-flex flex-wrap mb-3">
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">type</span>
                                    <input name="ri_type" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$rights[$scientificinfos[$resources[$i]["scientific_info"]]["rights"]]["type"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">holders</span>
                                    <select name="ri_holders" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($profiles as $key => $value) : ?>
                                            <option <?php if($value == $rights[$scientificinfos[$resources[$i]["scientific_info"]]["rights"]]["holders"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$rights[$scientificinfos[$resources[$i]["scientific_info"]]["rights"]]["holders"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h5>
                            Analysis :
                        </h5>
                        <div class="d-flex flex-wrap mb-3">
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">creation_date</span>
                                    <input name="a_creation_date" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$analysis[$scientificinfos[$resources[$i]["scientific_info"]]["analysis"]]["creation_date"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">analysis_date</span>
                                    <input name="a_analysis_date" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$analysis[$scientificinfos[$resources[$i]["scientific_info"]]["analysis"]]["analysis_date"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">revision_date</span>
                                    <input name="a_revision_date" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$analysis[$scientificinfos[$resources[$i]["scientific_info"]]["analysis"]]["revision_date"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">author</span>
                                    <select name="a_author" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($profiles as $key => $value) : ?>
                                            <option <?php if($value == $analysis[$scientificinfos[$resources[$i]["scientific_info"]]["analysis"]]["author"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$analysis[$scientificinfos[$resources[$i]["scientific_info"]]["analysis"]]["author"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">revision_author</span>
                                    <select name="a_revision_author" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($profiles as $key => $value) : ?>
                                            <option <?php if($value == $analysis[$scientificinfos[$resources[$i]["scientific_info"]]["analysis"]]["revision_author"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$analysis[$scientificinfos[$resources[$i]["scientific_info"]]["analysis"]]["revision_author"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">description_author</span>
                                    <select name="a_description_author" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($profiles as $key => $value) : ?>
                                            <option <?php if($value == $analysis[$scientificinfos[$resources[$i]["scientific_info"]]["analysis"]]["description_author"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$analysis[$scientificinfos[$resources[$i]["scientific_info"]]["analysis"]]["description_author"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">transcript_author</span>
                                    <select name="a_transcript_author" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($profiles as $key => $value) : ?>
                                            <option <?php if($value == $analysis[$scientificinfos[$resources[$i]["scientific_info"]]["analysis"]]["transcript_author"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$analysis[$scientificinfos[$resources[$i]["scientific_info"]]["analysis"]]["transcript_author"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h5>
                            Genetic :
                        </h5>
                        <div class="d-flex flex-wrap mb-3">
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">state</span>
                                    <input name="g_state" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$genetics[$scientificinfos[$resources[$i]["scientific_info"]]["genetic"]]["state"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">relation</span>
                                    <input name="g_relation" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$genetics[$scientificinfos[$resources[$i]["scientific_info"]]["genetic"]]["relation"]?>" style="width: fit-content">
                                </div>
                            </div>
                        </div>
                        <h5>
                            Publication :
                        </h5>
                        <div class="d-flex flex-wrap mb-3">
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">title</span>
                                    <input name="p_title" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$publications[$scientificinfos[$resources[$i]["scientific_info"]]["publication"]]["title"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">type</span>
                                    <input name="p_type" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$publications[$scientificinfos[$resources[$i]["scientific_info"]]["publication"]]["type"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">periodicity</span>
                                    <input name="p_periodicity" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$publications[$scientificinfos[$resources[$i]["scientific_info"]]["publication"]]["periodicity"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">pnumber</span>
                                    <input name="p_pnumber" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$publications[$scientificinfos[$resources[$i]["scientific_info"]]["publication"]]["pnumber"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">location</span>
                                    <select name="p_location" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($locations as $key => $value) : ?>
                                            <option <?php if($value == $publications[$scientificinfos[$resources[$i]["scientific_info"]]["publication"]]["location"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$publications[$scientificinfos[$resources[$i]["scientific_info"]]["publication"]]["location"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">director</span>
                                    <select name="p_director" class="form-control w-auto" aria-describedby="basic-addon1" value="Spain">
                                        <?php foreach ($profiles as $key => $value) : ?>
                                            <option <?php if($value == $publications[$scientificinfos[$resources[$i]["scientific_info"]]["publication"]]["director"]) echo "selected";?> value=<?= $key ?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                        <option <?php if(!$publications[$scientificinfos[$resources[$i]["scientific_info"]]["publication"]]["director"]) echo "selected";?> value=""></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h5>
                            Data :
                        </h5>
                        <div class="d-flex flex-wrap mb-3">
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">type</span>
                                    <input name="d_type" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["data"]["type"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">support</span>
                                    <input name="d_support" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["data"]["support"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">state</span>
                                    <input name="d_state" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["data"]["state"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">x</span>
                                    <input name="d_x" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["data"]["x"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">y</span>
                                    <input name="d_y" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["data"]["y"]?>" style="width: fit-content">
                                </div>
                            </div>
                        </div>
                        <h5>
                            File :
                        </h5>
                        <div class="d-flex flex-wrap mb-3">
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">name</span>
                                    <input name="f_name" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["file"]["name"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">extension</span>
                                    <input name="f_extension" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["file"]["extension"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">ko_size</span>
                                    <input name="f_ko_size" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["file"]["ko_size"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">url</span>
                                    <input name="f_url" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["file"]["url"]?>" style="width: fit-content">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-3 w-auto mx-3 ">
                                    <span class="input-group-text" id="basic-addon1" style="width: fit-content">last_consultation</span>
                                    <input name="f_last_consultation" type="text" class="form-control w-auto" aria-describedby="basic-addon1" value="<?=$resources[$i]["file"]["last_consultation"]?>" style="width: fit-content">
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-wrap justify-content-between">
                            <a href="../dependencies/delete_resource.php?lang=en&resource=<?= $resources[$i]["id"] ?><?= $pageparameters ?>" class="btn btn-danger ms-3">Delete</a>
                            <button type="submit" class="btn btn-primary me-3">Save</button>
                        </div>
                    </form>
                </div>
            </div>
            <?php endfor; ?>
        </div>
    </div>
    <nav class="d-flex justify-content-center mt-3">
        <ul class="pagination">
            <!-- Lien vers la page précédente (désactivé si on se trouve sur la 1ère page) -->
            <li class="page-item <?= ($currentPage == 1) ? "disabled" : "" ?>">
                <a href="./edit_resource.php?page=<?= ($currentPage-1) . $pageparameters ?>" class="page-link">Previous</a>
            </li>
            <?php for($page = $currentPage-5; $page <= $currentPage+5 && $page <= $pages; $page++): if ($page <= 0) continue?>
                <!-- Lien vers chacune des pages (activé si on se trouve sur la page correspondante) -->
                <li class="page-item <?= ($currentPage == $page) ? "active" : "" ?>">
                    <a href="./edit_resource.php?page=<?= $page . $pageparameters?>" class="page-link"><?= $page ?></a>
                </li>
            <?php endfor ?>
            <!-- Lien vers la page suivante (désactivé si on se trouve sur la dernière page) -->
            <li class="page-item <?= ($currentPage == $pages) ? "disabled" : "" ?>">
                <a href="./edit_resource.php?page=<?= ($currentPage+1) . $pageparameters ?>" class="page-link">Next</a>
            </li>
        </ul>
    </nav>
</div>




</body>

</html>


