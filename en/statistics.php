<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Xirgu's database</title>
    <?php
    session_start();
    if (!isset($_SESSION['id'])){
        http_response_code(401);
        die('Forbidden');
    }
    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/css.php";
    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/en_bdd.php";
    $query = $dbEN->prepare("SELECT p.name as author, COUNT(*) as resource
                                        FROM ressource r, contributors c, profile p
                                        WHERE r.contributors = c.id
                                        AND c.author = p.id
                                        AND p.name IS NOT NULL
                                        GROUP BY p.name;");
    $query->execute();
    $resourceByAuthor = $query->fetchAll();
    $query = $dbEN->prepare("SELECT d.state, COUNT(*) as resource
                                        FROM ressource r, data d
                                        WHERE r.data_info = d.id
                                        AND d.state IS NOT NULL
                                        GROUP BY d.state;");
    $query->execute();
    $resourceByCondition = $query->fetchAll();

    $query = $dbEN->prepare("SELECT l.country, COUNT(*) as resource
                                        FROM ressource r, location l
                                        WHERE r.geographical_context = l.id
                                        AND l.country IS NOT NULL
                                        GROUP BY l.country;");
    $query->execute();
    $resourceByCountry = $query->fetchAll();


    ?>
</head>

<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/nav_en.php"?>

<div class="container">
    <div class="card mt-3">
        <div class="card-body">
            <h5 class="card-title">Statistics Author Associated To Resources</h5>
            <canvas id="resourceByAuthor"></canvas>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <h5 class="card-title">Statistics Resources State</h5>
            <canvas id="resourceByCondition"></canvas>
        </div>
    </div>


    <div class="card mt-3">
        <div class="card-body">
            <h5 class="card-title">Statistics Country Associated To Resources</h5>
            <canvas id="resourceByCountry"></canvas>
        </div>
    </div>


</div>
</body>



<script>
    const colorScheme = [
        "rgb(236,204,104)","rgb(255,127,80)","rgb(255,107,129)","rgb(164,176,190)",
        "rgb(255,165,2)","rgb(255,99,72)","rgb(255,71,87)","rgb(116,125,140)",
        "rgb(123,237,159)","rgb(112,161,255)","rgb(83,82,237)","rgb(238,90,36)",
        "rgb(46,213,115)","rgb(30,144,255)","rgb(55,66,250)","rgb(196,229,56)",
        "rgba(255,99,132)","rgb(30,55,177)","rgb(32,174,123)","rgb(218,245,18)",
        "rgb(255,43,87)","rgb(92,202,3)","rgb(166,3,219)","rgb(8,212,190)",
        "rgb(31,23,26)","rgb(204,252,136)","rgb(136,5,33)","rgb(65,65,65)",
        "rgb(255,17,17)","rgb(110,39,56)","rgb(2,28,105)","rgb(175,117,243)",
        "rgb(3,76,6)","rgb(114,112,5)","rgb(248,86,8)","rgb(140,255,103)",
        "rgb(9,174,118)","rgb(226,11,11)","rgb(60,110,255)","rgb(203,136,149)"
    ]

    const colorSchemeBackground = [
        "rgb(236,204,104,0.2)","rgb(255,127,80,0.2)","rgb(255,107,129,0.2)","rgb(164,176,190,0.2)",
        "rgb(255,165,2,0.2)","rgb(255,99,72,0.2)","rgb(255,71,87,0.2)","rgb(116,125,140,0.2)",
        "rgb(123,237,159,0.2)","rgb(112,161,255,0.2)","rgb(83,82,237,0.2)","rgb(238,90,36,0.2)",
        "rgb(46,213,115,0.2)","rgb(30,144,255,0.2)","rgb(55,66,250,0.2)","rgb(196,229,56,0.2)",
        "rgba(255,99,132,0.2)","rgb(30,55,177,0.2)","rgb(32,174,123,0.2)","rgb(218,245,18,0.2)",
        "rgb(255,43,87,0.2)","rgb(92,202,3,0.2)","rgb(166,3,219,0.2)","rgb(8,212,190,0.2)",
        "rgb(31,23,26,0.2)","rgb(204,252,136,0.2)","rgb(136,5,33,0.2)","rgb(65,65,65,0.2)",
        "rgb(255,17,17,0.2)","rgb(110,39,56,0.2)","rgb(2,28,105,0.2)","rgb(175,117,243,0.2)",
        "rgb(3,76,6,0.2)","rgb(114,112,5,0.2)","rgb(248,86,8,0.2)","rgb(140,255,103,0.2)",
        "rgb(9,174,118,0.2)","rgb(226,11,11,0.2)","rgb(60,110,255,0.2)","rgb(203,136,149,0.2)"
    ]


    new Chart(document.getElementById("resourceByAuthor"), {
        type: 'bar',
        data: {
            "labels": [
                <?php
                foreach ($resourceByAuthor as $row){
                    echo '"'. $row["author"] . '",';
                }
                ?>],
            "datasets": [{
                "label": "Number of resource by author",
                "data": [
                    <?php
                    foreach ($resourceByAuthor as $row){
                        echo '"'. $row["resource"] . '",';
                    }
                    ?>],
                "fill": false,
                "backgroundColor": colorSchemeBackground,
                "borderColor": colorScheme,
                "borderWidth": 1
            }]
        },
        "options": {"scales": {"yAxes": [{"ticks": {"beginAtZero": true}}]}}
    });

    new Chart(document.getElementById("resourceByCondition"), {
        type: 'bar',
        data: {
            "labels": [
                <?php
                foreach ($resourceByCondition as $row){
                    echo '"'. $row["state"] . '",';
                }
                ?>],
            "datasets": [{
                "label": "Number of resource by resource condition",
                "data": [
                    <?php
                    foreach ($resourceByCondition as $row){
                        echo '"'. $row["resource"] . '",';
                    }
                    ?>],
                "fill": false,
                "backgroundColor": colorSchemeBackground,
                "borderColor": colorScheme,
                "borderWidth": 1
            }]
        },
        "options": {"scales": {"yAxes": [{"ticks": {"beginAtZero": true}}]}}
    });

    new Chart(document.getElementById("resourceByCountry"), {
        type: 'bar',
        data: {
            "labels": [
                <?php
                foreach ($resourceByCountry as $row){
                    echo '"'. $row["country"] . '",';
                }
                ?>],
            "datasets": [{
                "label": "Number of resource by resource condition",
                "data": [
                    <?php
                    foreach ($resourceByCountry as $row){
                        echo '"'. $row["resource"] . '",';
                    }
                    ?>],
                "fill": false,
                "backgroundColor": colorSchemeBackground,
                "borderColor": colorScheme,
                "borderWidth": 1
            }]
        },
        "options": {"scales": {"yAxes": [{"ticks": {"beginAtZero": true}}]}}
    });
</script>

</html>


