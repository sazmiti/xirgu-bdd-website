<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Xirgu's database</title>
    <?php
    session_start();
    if (!isset($_SESSION['id'])){
        http_response_code(401);
        die('Forbidden');
    }
    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/css.php";
    include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/esp_bdd.php";
    $query = $dbESP->prepare("SELECT COUNT(*)
                                    FROM ressource;");
    $query->execute();
    $nbResources = $query->fetchAll(PDO::FETCH_ASSOC);
    $query = $dbESP->prepare("SELECT DISTINCT p.*
                                    FROM ressource r, contributors c, Profile p
                                    WHERE r.contributors = c.id
                                    AND c.responsible_scientist = p.id;");
    $query->execute();
    $scientists = $query->fetchAll(PDO::FETCH_ASSOC);

    $query = $dbESP->prepare("SELECT SUM(f.ko_size) as total_file_ko_size
                                    FROM File f;");
    $query->execute();
    $totalSizeOfPicture = $query->fetchAll(PDO::FETCH_ASSOC);

    $query = $dbESP->prepare("SELECT COUNT(ressource_date), MIN(ressource_date), MAX(ressource_date)
                                    FROM (SELECT ressource_date
                                            FROM ressource
                                            WHERE ressource_date IS NOT NULL
                                            UNION
                                            SELECT ressource_date_inter
                                            FROM ressource
                                            WHERE ressource_date_inter IS NOT NULL) as dates;");
    $query->execute();
    $dateInformation = $query->fetchAll(PDO::FETCH_ASSOC);

    $query = $dbESP->prepare("SELECT COUNT(DISTINCT ko_size), MAX(ko_size), MIN(ko_size)
                                    FROM file
                                    WHERE ko_size IS NOT NULL;");
    $query->execute();
    $sizeInformation = $query->fetchAll(PDO::FETCH_ASSOC);

    $query = $dbESP->prepare("SELECT x, y
                                    FROM (SELECT MAX(x*y) as max, MIN(x*y) as min
                                          FROM data) as maxmin,
                                          data
                                    WHERE x*y = maxmin.max
                                    OR x*y = maxmin.min;");
    $query = $dbESP->prepare("SELECT MAX(x*y) as max, MIN(x*y) as min
                                          FROM data");
    $query->execute();
    $dimensionInformation = $query->fetchAll(PDO::FETCH_ASSOC);

    $query = $dbESP->prepare("SELECT COUNT(DISTINCT (x, y))
                                    FROM data
                                    WHERE x IS NOT NULL
                                    AND y IS NOT NULL;");
    $query->execute();
    $dimensionDiff = $query->fetchAll(PDO::FETCH_ASSOC);


    ?>
</head>

<body>
<?php include_once $_SERVER["DOCUMENT_ROOT"]."/dependencies/nav_esp.php"?>

<div class="container">
    <div class="card mt-3">
        <div class="card-body">
            <h1 class="card-title">Dashboard</h1>
            <ul>
                <li><p><b>Number of resources :</b> <?= $nbResources[0]["count"] ?></p></li>
            </ul>
            <h3>Scientists</h3>
            <ul>
                <?php foreach ($scientists as $scientist): ?>
                    <li><p><b><?= $scientist["name"] ?></b> - <?= $scientist["role"] ?> (<?= $scientist["organization"] ?> - <em><?= $scientist["grp"] ?></em>)</p></li>
                <?php endforeach; ?>
            </ul>
            <h3>Information on pictures</h3>
            <ul>
                <li><p>Only <b><?= $sizeInformation[0]["count"] ?></b> of <b><?= $nbResources[0]["count"] ?></b> has file size information</p></li>
                <ul>
                    <li><p><b>Total size of all the pictures :</b> <em><?= $totalSizeOfPicture[0]["total_file_ko_size"] ?> ko</em><p class="blockquote-footer">Warning some data is missing</p></em></p></li>
                    <li><p><b>Max file size :</b> <?= $sizeInformation[0]["max"] ?> ko</p></li>
                    <li><p><b>Min file size :</b> <?= $sizeInformation[0]["min"] ?> ko</p></li>
                </ul>
                <li><p>Only <b><?= $dimensionDiff[0]["count"] ?></b> of <b><?= $nbResources[0]["count"] ?></b> has file dimension information</p></li>
                <ul>
                    <li><p><b>Max file dimension :</b> <?= $dimensionInformation[0]["max"] ?> px</p></li>
                    <li><p><b>Min file dimension :</b> <?= $dimensionInformation[0]["min"] ?> px</p></li>
                </ul>
            </ul>
            <h3>Historical Information from dates</h3>
            <ul>
                <li><p>Only <b><?= $dateInformation[0]["count"] ?></b> of <b><?= $nbResources[0]["count"] ?></b> has file size information</p></li>
                <ul>
                    <li><p><b>Nearest date from us :</b> <?= $dateInformation[0]["max"] ?></p></li>
                    <li><p><b>Further date from us  :</b> <?= $dateInformation[0]["min"] ?></p><p class="blockquote-footer">This date seems to be an error</p></li>
                </ul>
            </ul>
        </div>
    </div>


</div>
</body>


</html>


